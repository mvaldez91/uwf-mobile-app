// this sets the background color of the master UIView (when there are no windows/tab groups on it)
Titanium.UI.setBackgroundColor('#000');




// create tab group
var tabGroup = Titanium.UI.createTabGroup();

var eventWin = Ti.UI.createWindow({ 
   modal: false
    
}); 

tabGroup.exitOnClose = true;


//
// create base UI tab and root window
//
var win1 = Titanium.UI.createWindow({  
    title:'Account Management',
    backgroundColor:'#fff'
});

win1.orientationModes = [Titanium.UI.PORTRAIT, Titanium.UI.UPSIDE_PORTRAIT];
var tab1 = Titanium.UI.createTab({  
    icon:'KS_nav_views.png',
    title:'Account',
    window:win1
});

win1.setBackgroundImage('/images/background.png');


var scheduleButton = Titanium.UI.createButton({
   
   backgroundImage: '/images/schedule.png',
   top: 10,
   width: 280,
   height: 80
});

scheduleButton.addEventListener('click',function(e)
{
   eventWin.setTitle("Schedule")
   eventWin.url = 'schedule.js'
   tab1.open(eventWin);
   
});

win1.add(scheduleButton);



 


var gradeButton = Titanium.UI.createButton({
   
   backgroundImage: '/images/grades.png',
   top: 100,
   width: 280,
   height: 80
});

gradeButton.addEventListener('click',function(e)
{
   eventWin.setTitle("Grades")
   eventWin.url = 'grades.js'
   tab1.open(eventWin);
   
});

win1.add(gradeButton);






var holdButton = Titanium.UI.createButton({
   
   backgroundImage: '/images/holds.png',
   top: 190,
   width: 280,
   height: 80
});

holdButton.addEventListener('click',function(e)
{
   eventWin.setTitle("Holds")
   eventWin.url = 'holds.js'
   tab1.open(eventWin);
   
});

win1.add(holdButton);




/**
 * This creates the elements required
 * for the logout
 */



var logoutButton = Titanium.UI.createButton({
   title: 'Logout',
   top: 450,
   width: 300,
   height: 100
});

logoutButton.addEventListener('click',function(e)
{
   
   eventWin.setTitle("Logout")
   eventWin.url = 'logout.js'
   tab1.open(eventWin);
   
});

win1.add(logoutButton);






//
// create controls tab and root window
//
var win2 = Titanium.UI.createWindow({  
    title:'Tab 2',
    backgroundImage: '/images/background.png',
});
var tab2 = Titanium.UI.createTab({  
    icon:'KS_nav_ui.png',
    title:'Social',
    window: win2
  
   
});



/**
 * Creates elements to create the
 * twitter window
 */
var twitterWin = Ti.UI.createWindow({ 
   modal: false,
   title: "UWF Twitter",
   url: 'twitternew.js'
    
});

var twitterButton = Titanium.UI.createButton({
   
   backgroundImage: '/images/twitter.png',
   top: 90,
   width: 280,
   height: 80
});

twitterButton.addEventListener('click',function(e)
{
   
   tab2.open(twitterWin);
   
});
win2.add(twitterButton);



var mapButton = Titanium.UI.createButton({
   backgroundImage: '/images/campusmap.png',
   top: 10,
   width: 280,
   height: 80
});

mapButton.addEventListener('click',function(e)
{
   eventWin.setTitle("Campus Map")
   eventWin.url = 'campusmap.js'
   tab2.open(eventWin);  
});
win2.add(mapButton);



var youtubeButton = Titanium.UI.createButton({
   
   backgroundImage: '/images/youtube.png',
   top: 170,
   width: 280,
   height: 80
});



youtubeButton.addEventListener('click',function(e)
{
   eventWin.url = 'youtube.js'
   tab3.open(eventWin);
   
});
win2.add(youtubeButton);



var instaButton = Titanium.UI.createButton({
   
   backgroundImage: '/images/instagram.png',
   top: 250,
   width: 280,
   height: 80
});



instaButton.addEventListener('click',function(e)
{
   eventWin.url = 'instagram.js'
   tab3.open(eventWin);
   
});
win2.add(instaButton);



var faceButton = Titanium.UI.createButton({
   
   backgroundImage: '/images/facebook.png',
   top: 330,
   width: 280,
   height: 80
});



faceButton.addEventListener('click',function(e)
{
   eventWin.url = 'facebook.js'
   tab3.open(eventWin);
   
});
win2.add(faceButton);



var linkedButton = Titanium.UI.createButton({
   
   backgroundImage: '/images/linkedin.png',
   top: 410,
   width: 280,
   height: 80
});



linkedButton.addEventListener('click',function(e)
{
   eventWin.url = 'linkedin.js'
   tab3.open(eventWin);
   
});
win2.add(linkedButton);






/**
 * This is the financial
 * tab
 */

var win3 = Titanium.UI.createWindow({  
    title:'Tab 3',
    backgroundImage: '/images/background.png',
});
var tab3 = Titanium.UI.createTab({  
    icon:'KS_nav_ui.png',
    title:'Finances',
    window: win3
  
   
});




var balanceButton = Titanium.UI.createButton({
   
   backgroundImage: '/images/balance.png',
   top: 10,
   width: 280,
   height: 80
});



balanceButton.addEventListener('click',function(e)
{
   eventWin.url = 'balance.js'
   tab3.open(eventWin);
   
});
win3.add(balanceButton);




var finaidButton = Titanium.UI.createButton({
   
   backgroundImage: '/images/finaid.png',
   top: 90,
   width: 280,
   height: 80
});



finaidButton.addEventListener('click',function(e)
{
   eventWin.url = 'finaid.js'
   tab3.open(eventWin);
   
});
win3.add(finaidButton);





var nautiButton = Titanium.UI.createButton({
   
   backgroundImage: '/images/nautibal.png',
   top: 90,
   width: 280,
   height: 80
});



nautiButton.addEventListener('click',function(e)
{
   eventWin.url = 'nautibalance.js'
   tab3.open(eventWin);
   
});
win3.add(nautiButton);








//
//  add tabs
//
tabGroup.addTab(tab1);  
tabGroup.addTab(tab2);  
tabGroup.addTab(tab3);

// open tab group
tabGroup.open();


/**
 * Creates the action bar for the menu
 */
tabGroup.addEventListener('open', function(e) {
    var actionBar = tabGroup.getActivity().actionBar; // "activity" is an undocumented Android property for TabGroup
    if (actionBar) {
        actionBar.setDisplayHomeAsUp(false); // works
        
        actionBar.setTitle('UWF Mobile App'); // hard-coded text just to be sure
    }
  	
     
});





/**
 * Creates the ActionBar for the Twitter Window
 * Adds an eventListener for the twitter for when it is focus
 * the ActionBar will be created
 */
twitterWin.addEventListener('focus', function(e){
	

 if (Ti.Platform.osname === "android") {
   twitterWin.activity.onCreateOptionsMenu = function(e) {
        
        if (Ti.Platform.osname === "android") {
     var actionBar = twitterWin.getActivity().actionBar;
     if (actionBar) {
     
     actionBar.title = 'UWF Twitter'
     actionBar.displayHomeAsUp = true;
    
    actionBar.onHomeIconItemSelected = function(e) {
     var eventWin = Ti.UI.createWindow({ 
   url:'menu.js',
   
   })

   eventWin.open({animated:false});
   twitterWin.close();
   tabGroup.close();
     };
    
     }
     }
        
    };
  };
 });
 
 

/**
 * Closes out the rogue windows. So when a user presses the back button
 * they will not be brought to a blank black window
 */

    
eventWin.addEventListener('focus', function(e){
 
    eventWin.close();
    
    });


