
var twitterWindow = Ti.UI.createWindow({  
    title:'UWF Twitter',
    
});

/**
 * Creates a http client to get the twitter feed
 */
var twitterUserName = "UWF"; //UWF Twitter Feed
var httpClient = Ti.Network.createHTTPClient();
httpClient.timeout = 100000;
httpClient.open("GET","http://api.twitter.com/1/statuses/" + 
    "user_timeline.json?count=20&include_rts=1&screen_name=" + twitterUserName);

/**
 * Adds a Loading Indicator when the twitter feed is being retrieved
 * Allows the User to have an awareness that the Application is working and not hanged up on loading the twitter feed
 */ 
var actInd = Titanium.UI.createActivityIndicator({
    top:100,
    height:50,
    font: {fontFamily:'Helvetica Neue', fontSize:25,fontWeight:'bold'},
    color: 'white',
    message: 'Loading...',
    width: 210
}); 

actInd.show();
Titanium.UI.currentWindow.add(actInd);

 
 
var twitterData = [];
httpClient.onload = function() {
	actInd.hide()
    try {
        var tweets = eval('(' + this.responseText + ')');
        for (var i=0; i < tweets.length; i++) {
 
            var tweetText = tweets[i].text;
            var user = tweets[i].user.screen_name;
            var avatar = tweets[i].user.profile_image_url;
            var created_at = tweets[i].created_at;
 
            var row = Ti.UI.createTableViewRow({hasChild:false,
                height:'auto', width:'auto'});
 
            var postView = Ti.UI.createView({
                height:'auto',
                layout:'vertical',
                left:5,
                top:5,
                bottom:5,
                right:5,
                
            });
 
            var avatarImageView = Ti.UI.createImageView({
                image:avatar,
                left:0,
                top:0,
                height:90,
                width:90
            });
 
            postView.add(avatarImageView);
           
 
            var userLabel = Ti.UI.createLabel({
                text:user,
                left:110,
                width:120,
                top:-89,
                bottom:2,
                height:'auto',
                textAlign:'left',
                color:'#4CA950',
                font:{fontFamily:'Trebuchet MS',fontSize:24,
                    fontWeight:'bold'}
            });
 
            postView.add(userLabel);
 
            var dateLabel = Ti.UI.createLabel({
                text:created_at,
                right:0,
                top:-35,
                bottom:2,
                height:18,
                textAlign:'right',
                width:110,
                color:'#4CA950',
                font:{fontFamily:'Trebuchet MS',fontSize:16}
            });
 
            postView.add(dateLabel);
 
            var tweetTextLabel = Ti.UI.createLabel({
                text:tweetText,
                left: 110,
                top:10,
                bottom:2,
                height:'auto',
                width:472,
                textAlign:'left',
                font:{fontSize:22},
                color: '#00AFDB'
            });
 
            postView.add(tweetTextLabel);
            row.add(postView);
            twitterData[i] = row;
        }
 
        var tableview = Titanium.UI.createTableView({data:twitterData,
            minRowHeight:120, allowsSelection: false});
        twitterWindow.add(tableview);
        twitterWindow.open();
    } catch(E) {
        alert('Could not load Twitter Feed at this time.');
    }
};
httpClient.send();

